import { Component, Input, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActionSheetController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
import { TabsInfoService } from 'src/app/services/noticias.service';

const { Browser } = Plugins;
const { Share } = Plugins;


@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.scss'],
})
export class NoticiasComponent implements OnInit {


  @Input('news') news: Array<any>
  @Input('loadData') loadData: any
  @Input('numPage') numPage: number
  @Input('tab') tab: string
  @Input('currentCategory') currentCategory: string



  constructor(public actionSheetController: ActionSheetController,private serviceFav:TabsInfoService) {
  }
  ngOnInit() {
  }

  async openBrowser(url){
    await Browser.open({ url: url });
  }


  async lanzarMenu(noticia) {

    const actionSheet = await this.actionSheetController.create({
      animated:true,
      buttons: [
        {
        text: 'Share',
        icon: 'share',
        handler:async () => {
          console.log('Share clicked');
          let shareRet = await Share.share({
            title: noticia.title,
            text: noticia.description,
            url: noticia.url,
            dialogTitle: 'Share with buddies'
          });
        }
      },
      {
        text: this.tab?'Delete Favorite':'Favorite',
        role: this.tab?'delete favorite':'favorite',
        icon: 'star',
        handler:async () => {
          if(this.tab){
            const deleted= await this.serviceFav.deleteFav(noticia)
            this.news=deleted
          }else{
            console.log('Fav clicked');
            const isAdd=await this.serviceFav.setFav(noticia)
          }
        }
      },
       {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    await actionSheet.present();
  }

scrollToTop() {
  getContent().scrollToTop();
}

}
function getContent() {
  return document.querySelector('ion-content');
}

