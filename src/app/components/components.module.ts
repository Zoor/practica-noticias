import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoticiasComponent } from './noticias/noticias.component';



@NgModule({
  declarations: [
    NoticiasComponent
  ],
  exports:[
    NoticiasComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
