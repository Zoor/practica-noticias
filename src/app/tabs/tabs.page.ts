import { Component, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { Tab1Page } from '../pages/tab1/tab1.page';
import { TabsPageRoutingModule } from './tabs-routing.module';
import { TabsPageModule } from './tabs.module';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor() {}
}
