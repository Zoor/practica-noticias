import { TestBed } from '@angular/core/testing';

import { TabsInfoService } from './noticias.service';

describe('TabsInfoService', () => {
  let service: TabsInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TabsInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
