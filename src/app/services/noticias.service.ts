import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class TabsInfoService {

  private _storage: Storage | null = null;

  favs: Array<any> = []

  constructor(private storage: Storage) {
    this.init()
  }

  async init() {
    const storage = await this.storage.create();
    this._storage = storage;
    this.loadFavs()
  }

  async loadFavs() {
    const arr = await this.storage.get('Favs');
    this.favs = arr || []
    
  }
  async setFav(fav) {
    const isRep=this.favs.filter(el=>fav.url===el.url)
    if(isRep[0]) return false
    this.favs.push(fav)
    await this.storage.set('Favs', this.favs);
    return true
  }

  async deleteFav(fav){
    const newFavs=this.favs.filter(el=>fav.url!==el.url)
    this.favs=newFavs
    await this.storage.set('Favs', this.favs);
    return this.favs

  }

  async cleanStorage(){
    this.storage.remove('Favs')
  }

}
