import { Component } from '@angular/core';
import { TabsInfoService } from 'src/app/services/noticias.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  news: any = []
  numPage: number = 2
  tab:string= 'tab3'


  constructor(private serviceFavs:TabsInfoService) {}

  async ngOnInit(){
    await this.serviceFavs.loadFavs()
    this.news=this.serviceFavs.favs
  }

  ionViewWillEnter() {
    this.tab='tab3'
    this.ngOnInit()
  }
  ionViewWillLeave(){
    this.tab=''
  }
  loadData(event){
    event.target.complete()
  }

}
