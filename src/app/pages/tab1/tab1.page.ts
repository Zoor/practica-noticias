import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {


  news: any = []
  numPage: number = 2

  constructor(private data: DataService, private http: HttpClient) { }

  ngOnInit() {
    this.data.getNews(1).toPromise().then((res: any) => {
      this.news = res.articles
    })
  }

  loadData(event) {
    setTimeout(() => {
      this.data.getNews(this.numPage).toPromise().then((res: any) => {
        const info = res.articles
        info.forEach((element) => {
          this.news.push(element)
        });
        this.numPage++
      })
      console.log('done')
      event.target.complete();
    }, 2000);
  }
  ionViewWillEnter() {
    this.ngOnInit()
  }


}
