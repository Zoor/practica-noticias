import { Component } from '@angular/core';
import { NoticiasComponent } from 'src/app/components/noticias/noticias.component';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  segments = ["BUSINESS", "ENTERTAINMENT", "GENERAL"]

  news: any = []
  numPage: number = 2
  currentCategory: string = "BUSINESS"


  constructor(private data: DataService, private noticias: NoticiasComponent) { }


  ngOnInit() {
    this.data.getNewsByCategory(1, this.currentCategory).toPromise().then((res: any) => {
      this.news = res.articles
    })
  }

  segmentChanged(e) {
    this.noticias.scrollToTop()
    this.currentCategory = e.detail.value
    this.data.getNewsByCategory(1, e.detail.value).toPromise().then((res: any) => {
      this.news = res.articles
    })

  }

  loadData(event) {

    setTimeout(() => {
      this.data.getNewsByCategory(this.numPage, this.currentCategory).toPromise().then((res: any) => {
        const info = res.articles
        info.forEach((element) => {
          this.news.push(element)
        });
        this.numPage++
      })
      console.log('done')
      event.target.complete();
    }, 2000);
  }

  ionViewWillEnter() {
    this.ngOnInit()
  }

}
